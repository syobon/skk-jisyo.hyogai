# 表外漢字熟語SKK辞書
表外漢字が使用された熟語の辞書です。それ已上でも已下でもありません。

## 参考
- [代用字による書き換え](https://sound.jp/mtnest/k/tojo-daiyo-.htm)
- [同音の漢字による書きかえ](https://ja.wikipedia.org/wiki/%E5%90%8C%E9%9F%B3%E3%81%AE%E6%BC%A2%E5%AD%97%E3%81%AB%E3%82%88%E3%82%8B%E6%9B%B8%E3%81%8D%E3%81%8B%E3%81%88)
